require_relative "routes/sessions"
require_relative "helpers"
require_relative "libs/mongo"
require_relative "routes/equipos"
require_relative "routes/signup"

require "digest/md5"

def to_md5(pass)
  return Digest::MD5.hexdigest(pass)
end

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  config.before(:suite) do
    # massa de teste que faz um drop no mongodb, exclui os dados e repoe novamente antes da execuçao
    users = [
      { name: "Gustavo Silva", email: "gustavo.silva@hotmail.com", password: to_md5("naotemsenha") },
      { name: "Cadastro Equipos", email: "cadastroequip@hotmail.com", password: to_md5("naotemsenha") },
      { name: "Locador da Fender", email: "locador@hotmail.com", password: to_md5("naotemsenha") },
      { name: "Locacao da Fender", email: "locacao@hotmail.com", password: to_md5("naotemsenha") },
      { name: "Lista Equipos", email: "listaequipos@hotmail.com", password: to_md5("naotemsenha") },
    ]

    MongoDB.new.drop_danger
    MongoDB.new.insert_users(users)
  end
end
