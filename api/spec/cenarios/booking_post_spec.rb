
describe "POST /equipos/{equipo_id}/bookings" do

    before(:all) do
        payload = { email: "locador@hotmail.com", password: "naotemsenha" }
        result = Sessions.new.login(payload)
        @locador_id = result.parsed_response["_id"]
    end

    context "solicitar locação" do
        before(:all) do
            # dado que "Email Locação" tem uma Fender Strato para locação
            result = Sessions.new.login({email: "locacao@hotmail.com", password: "naotemsenha"})
            locacao_id = result.parsed_response["_id"]

            fender = { 
                thumbnail: Helpers::get_thumb("fender-sb.jpg"), 
                name: "Fender Strato", 
                category: "Cordas", 
                price: 150,
            }
            MongoDB.new.remove_equipo(fender[:name], locacao_id)

            result = Equipos.new.create(fender, locacao_id)
            fender_id = result.parsed_response["_id"]

            # quando solicito a locação da fender do "Email Locação"
            @result = Equipos.new.booking(fender_id, @locador_id)
            
        end

        it "deve retornar 200" do
            expect(@result.code).to eql 200
        end

    end
end