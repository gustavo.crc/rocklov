#encoding: utf-8

describe "POST /equipos" do

    before(:all) do
        payload = { email: "cadastroequip@hotmail.com", password: "naotemsenha" }
        result = Sessions.new.login(payload)
        @user_id = result.parsed_response["_id"]
    end

    context "novo equipamento" do
        before(:all) do
            payload = { 
                thumbnail: Helpers::get_thumb("mic.jpg"), 
                name: "Microfone sem fio", 
                category: "Áudio e Tecnologia".force_encoding("ASCII-8BIT"), 
                price: 100,
            }

        MongoDB.new.remove_equipo(payload[:name], @user_id)

        @result = Equipos.new.create(payload, @user_id)
        end
           
        it "deve retornar 200" do
        expect(@result.code).to eql 200
        end
    end

    context "nao autorizado" do
        before(:all) do
            payload = { 
                thumbnail: Helpers::get_thumb("baixo.jpg"), 
                name: "Contra baixo", 
                category: "Cordas", 
                price: 85,
            }

        @result = Equipos.new.create(payload, nil)
        end
           
        it "deve retornar 401" do
        expect(@result.code).to eql 401
        end
    end
end