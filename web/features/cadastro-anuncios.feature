#language: pt

Funcionalidade: Cadastro de Anuncios
    Sendo usuario cadastrado no rocklov
    Quero cadastrar meus equipamentos
    Para que eu possa disponibilizados para locação

    Contexto: Login
        * Login com "gustavo.carranca@hotmail.com" e "naotemsenha"

    Cenario: Novo Equipo

        Dado que acesso o formulario de cadastro de anuncios
            E que eu tenho o seguinte equipamento:
            | thumb     | fender-sb.jpg |
            | nome      | Fender Strato |
            | categoria | Cordas        |
            | preco     | 200           |
        Quando submeto o cadastro desse item
        Entao devo ver esse item no meu Dashboard

    Esquema do Cenario: Tentativa de Cadastro de Anuncios
        Dado que acesso o formulario de cadastro de anuncios
            E que eu tenho o seguinte equipamento:
            | thumb     | <foto>        |
            | nome      | <equipamento> |
            | categoria | <categoria>   |
            | preco     | <valor>       |
        Quando submeto o cadastro desse item
        Então vejo a mensagem de alerta: "<saida>"

        Exemplos:
            | foto             | equipamento     | categoria | valor | saida                                |
            |                  | Violão de Nylon | Cordas    | 100   | 📷 Adicione uma foto no seu anúncio! |
            | amp.jpg          |                 | Outros    | 300   | 🎸 Informe a descrição do anúncio!   |
            | mic.jpg          | Microfone       |           | 50    | 🤭 Informe a categoria               |
            | sanfona.jpg      | Sanfona         | Teclas    |       | 💰 Informe o valor da diária         |
            | violao-nylon.jpg | Violao de Nylon | Cordas    | FdsE  | O valor da diária deve ser numérico! |
            | violao-nylon.jpg | Violao de Nylon | Cordas    | 100e  | O valor da diária deve ser numérico! |

