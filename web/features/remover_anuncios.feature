#language: pt

Funcionalidade: Remover Anúncios
    Sendo um anunciante que possui um equipamento indesejado
    Quero poder remover esse anúncio
    Para que eu possa manter o meu dashboard atualizado

    Contexto: Login
        * Login com "douglas@hotmail.com" e "naotemsenha"


    @temp
    Cenario: Remover um Anúncio

        Dado que eu tenho um anúncio indesejado:
            | thumb     | telecaster.jpg |
            | nome      | Telecaster     |
            | categoria | Cordas         |
            | preco     | 50             |
        Quando eu solicito a exclusão desse item
            E confirmo e exclusão
        Entao não devo ver esse item no meu dashboard
    
    @temp
    Cenario: Desistir da Exclusão

        Dado que eu tenho um anúncio indesejado:
            | thumb     | conga.jpg |
            | nome      | Conga     |
            | categoria | Outros    |
            | preco     | 100       |
        Quando eu solicito a exclusão desse item
            Mas não confirmo e exclusão
        Entao esse item deve permanecer no meu dashboard