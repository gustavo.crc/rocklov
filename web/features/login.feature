#language: pt

Funcionalidade: Login
Sendo um usuário cadastrado
Quero acessar o sistema da Rocklov
Para que eu possa anunciar meus equipamentos musicais

Cenario: Login do usuario

    Dado que acesso a página principal
    Quando submeto minhas credenciais com "gustavo.silva@hotmail.com" e "naotemsenha"
    Então sou redirecionado para o Dashboard

Esquema do Cenario: Tentar logar

    Dado que acesso a página principal
    Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
    Então vejo a mensagem de alerta: "<mensagem_output>"

    Exemplos:
    |email_input          |senha_input|mensagem_output                 |
    |gustavo@hotmail.com  |temsenha   |Usuário e/ou senha inválidos.   |
    |gustavo02@hotmail.com|naotemsenha|Usuário e/ou senha inválidos.   |
    |gustavo01@hotmail.com|naotemsenha|Usuário e/ou senha inválidos.   |
    |                     |naotemsenha|Oops. Informe um email válido!  |
    |gustavo01@hotmail.com|           |Oops. Informe sua senha secreta!|