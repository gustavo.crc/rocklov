Before do
    @signup_page = SignUpPage.new
    @alert = Alert.new
    @login_page = LoginPage.new
    @dash_page = DashPage.new
    @equipos_page = EquiposPage.new

    # page.driver.brownser.manage.window.maximize
    page.current_window.resize_to(1440, 900)
end

After do
    temp_shot = page.save_screenshot("logs/temp_screenshot.png")

    Allure.add_attachment(
        name: "Screenshot",
        type: Allure::ContentType::PNG,
        source: File.open(temp_shot)
    )
end