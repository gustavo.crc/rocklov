class DashPage
    include Capybara::DSL

    def on_dash?
        return page.has_css?(".dashboard")
    end

    def go_to_equipo_form
        click_button "Criar anúncio"
    end

    def equipo_list
        return find(".equipo-list")
    end

    def has_no_equipo?(name)
        return page.has_no_css?(".equipo-list li", text: name)
    end

    def request_remove(name)
        equipo = find(".equipo-list li", text: name)
        equipo.find(".delete-icon").click
    end

    def order
        return find(".notifications p")
    end

    def order_actions(name)
        return page.has_css?(".notifications button", text: name)
    end

end