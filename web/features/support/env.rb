require "capybara"
require "capybara/cucumber"
require "faker"
require "allure-cucumber"

CONFIG = YAML.load_file(File.join(Dir.pwd, "features/support/config/#{ENV["CONFIG"]}"))

case ENV["BROWSER"]
when "firefox"
 @driver = :selenium
when "fire_headless"
 @driver = :selenium_headless 
when "chrome"
 @driver = :selenium_chrome 
when "chrome_headless"
    Capybara.register_driver :selenium_chrome_headless do |app|
        Capybara::Selenium::Driver.load_selenium
        browser_options = ::Selenium::WebDriver::Chrome::Options.new.tap do |opts|
          opts.args << '--headless'
          opts.args << '--disable-gpu'
          opts.args << '--disable-site-isolation-trials'
          opts.args << '--no-sandbox'
          opts.args << '--disable-dev-shm-usage'
        end
        Capybara::Selenium::Driver.new(app, browser: :chrome, options: browser_options)
    end
 @driver = :selenium_chrome_headless
else
 raise "Navegador não encontrado para a execução do teste!"
end

Capybara.configure do |config|
    config.default_driver = @driver
    config.app_host = "http://rocklov-web:3000"
    config.default_max_wait_time = 10
end

AllureCucumber.configure do |config|
    config.results_directory = "/logs"
    config.clean_results_directory = true
end