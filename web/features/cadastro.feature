#language: pt


Funcionalidade: Cadastro
    Sendo um músico que possui equipamentos musicais
    Quero fazer o meu cadastro no RockLov
    Para que eu possa disponibilizá-los para locação

    Cenario: Fazer cadastro

        Dado que acesso a página de cadastro
        Quando submeto o seguinte formulario de cadastro:
            | nome             | email               | senha       |
            | Gustavo da Silva | gustavo@hotmail.com | naotemsenha |
        Então sou redirecionado para o Dashboard

    Esquema do Cenario: Tentativa de Cadastro

        Dado que acesso a página de cadastro
        Quando submeto o seguinte formulario de cadastro:
            | nome         | email         | senha         |
            | <nome_input> | <email_input> | <senha_input> |
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | nome_input    | email_input         | senha_input | mensagem_output                  |
            |               | gustavo@hotmail.com | naotemsenha | Oops. Informe seu nome completo! |
            | Gustavo Silva |                     | naotemsenha | Oops. Informe um email válido!   |
            | Gustavo Silva | gustavo#hotmail.com | naotemsenha | Oops. Informe um email válido!   |
            | Gustavo Silva | gustavo%hotmail.com | naotemsenha | Oops. Informe um email válido!   |
            | Gustavo Silva | gustavoAhotmail.com | naotemsenha | Oops. Informe um email válido!   |
            | Gustavo Silva | gustavo*hotmail.com | naotemsenha | Oops. Informe um email válido!   |
            | Gustavo Silva | gustavo!hotmail.com | naotemsenha | Oops. Informe um email válido!   |
            | Gustavo Silva | gustavo@hotmail.com |             | Oops. Informe sua senha secreta! |
