#language: pt


Funcionalidade: Receber pedido de locação
    Sendo um anunciante que possui equipamentos cadastrados
    Desejo receber pedidos de locação
    Para que eu possa decidir se quero aprovar ou rejeitar

    Cenario: Receber pedido
        Dado que meu perfil de anunciante é "joaoanunciante@hotmail.com" e "naotemsenha"
            E que eu tenho o seguinte equipamento cadastrado:
            | thumb     | sanfona.jpg |
            | nome      | Sanfona     |
            | categoria | Teclas      |
            | preco     | 300         |
            E acesso o meu dashboard
        Quando "marialocataria@hotmail.com" e "naotemsenha" solicita a locação desse equipamento
        Entao devo ver a seguinte mensagem:
            """
            marialocataria@hotmail.com deseja alugar o equipamento: Sanfona em: DATA_ATUAL
            """
            E devo ver os links: "ACEITAR" e "REJEITAR" no pedido